type state = 'select'|'create';

export class SelectOrCreate {
    private $select: HTMLElement;
    private $create: HTMLElement;
    private $switchCreate: HTMLButtonElement;
    private $switchSelect: HTMLButtonElement;
    private $state: HTMLSelectElement;

    private state: state = 'select';

    public constructor(
        private $selectOrCreate: HTMLElement,
        dataAttribute: string = 'data-select-or-create',
    ) {
        this.$select = $selectOrCreate.querySelector(`[${dataAttribute}--select]`);
        this.$create = $selectOrCreate.querySelector(`[${dataAttribute}--create]`);
        this.$switchCreate = $selectOrCreate.querySelector(`[${dataAttribute}--switch-create]`);
        this.$switchSelect = $selectOrCreate.querySelector(`[${dataAttribute}--switch-select]`);
        this.$state = $selectOrCreate.querySelector(`[${dataAttribute}--state]`);
        this.$state.hidden = true;

        const selectHidden = this.$select.hidden;
        const createHidden = this.$create.hidden;

        if ((!createHidden) && selectHidden) {
            this.switch('create');
        } else {
            this.switch('select');
        }

        this.initListeners();
    }

    public switch(state: state|null = null): void {
        if (null === state) {
            state = 'select' === this.state ? 'create' : 'select';
        }

        this.$select.hidden = 'create' === state;
        this.$switchCreate.hidden = 'create' === state;
        this.$create.hidden = 'select' === state;
        this.$switchSelect.hidden = 'select' === state;
        this.$state.value = state;
    }

    private initListeners(): void {
        this.$switchCreate.addEventListener('click', () => {
            this.switch('create');
        });

        this.$switchSelect.addEventListener('click', () => {
            this.switch('select');
        });
    }
}
