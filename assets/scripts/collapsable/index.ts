import {Collapsable} from './collapsable';
import {CollapsableManager} from './collapsable-manager';

export {
    Collapsable,
    CollapsableManager,
}
