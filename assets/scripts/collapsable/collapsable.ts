export const COLLAPSER_ATTRIBUTE_REGISTERED = '_form_collapser_registered';

export class Collapsable {
    readonly $collapsable: HTMLElement;

    constructor(
        readonly $collapse: HTMLElement,
        readonly collapseAttributeName: string = 'data-form-collapse',
        readonly collapserAttributeName: string = 'data-form-collapser',
        readonly collapsableAttributeName: string = 'data-form-collapsable',
    ) {
        const selector = this.$collapse.attributes.getNamedItem(this.collapseAttributeName).value;
        this.$collapsable = $collapse.querySelector(selector);

        if (null === this.$collapsable) {
            throw new Error('No element found for selector "'+selector+'".');
        }

        if ('' === this.$collapsable.id) {
            throw new Error('Missing ID attribute for "'+selector+'".');
        }

        if (null === this.$collapsable.attributes.getNamedItem(this.collapsableAttributeName)) {
            throw new Error('Missing collapsable attribute "'+this.collapsableAttributeName+'" for "'+selector+'".');
        }

        this.registerCollapser($collapse);
    }

    public registerCollapser($section: HTMLElement = document.querySelector('body')): void {
        for (const $collapser of $section.querySelectorAll('['+this.collapserAttributeName+'="#'+this.$collapsable.id+'"]')) {
            if (COLLAPSER_ATTRIBUTE_REGISTERED in $collapser) {
                continue;
            }

            $collapser[COLLAPSER_ATTRIBUTE_REGISTERED] = true;

            $collapser.addEventListener('click', () => {
                const attr = this.$collapsable.attributes.getNamedItem(this.collapsableAttributeName);

                if ('true' === attr.value) {
                    attr.value = 'false';
                } else {
                    attr.value = 'true';
                }
            });
        }
    }
}
