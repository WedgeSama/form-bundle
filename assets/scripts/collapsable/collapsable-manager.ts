import {Collapsable, COLLAPSER_ATTRIBUTE_REGISTERED} from './collapsable';

export class CollapsableManager {
    private collapsables: Array<Collapsable> = [];

    public constructor(
        readonly collapseAttributeName: string = 'data-form-collapse',
        readonly collapserAttributeName: string = 'data-form-collapser',
        readonly collapsableAttributeName: string = 'data-form-collapsable',
    ) {}

    public scanForCollapsable($section: HTMLElement|string): void {
        if (typeof $section === 'string') {
            $section = document.querySelector<HTMLElement>($section);
        }

        this.registerCollapser($section);

        for (const $collapsable of [].slice.call($section.querySelectorAll<HTMLElement>('['+this.collapseAttributeName+']')) as Array<HTMLElement>) {
            this.registerCollapse($collapsable);
        }

        if (null !== $section.attributes.getNamedItem(this.collapseAttributeName)) {
            this.registerCollapse($section);
        }
    }

    public registerCollapser($section: HTMLElement|string = document.querySelector('body')): void {
        if (typeof $section === 'string') {
            $section = document.querySelector<HTMLElement>($section);
        }

        for (const collapsable of this.collapsables) {
            collapsable.registerCollapser($section);
        }
    }

    private alreadyCollapsable($element: HTMLElement): boolean {
        return COLLAPSER_ATTRIBUTE_REGISTERED in $element;
    }

    private registerCollapse($collapsable: HTMLElement): void {
        if (!this.alreadyCollapsable($collapsable)) {
            this.collapsables.push(new Collapsable(
                $collapsable,
                this.collapseAttributeName,
                this.collapserAttributeName,
                this.collapsableAttributeName
            ));
        }
    }
}
