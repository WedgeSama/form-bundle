import {CollectionEvent} from './collection-event';
import {htmlToElement, regxEscape} from '../utils';

/**
 * Represente a collection of element.
 */
export class Collection {
    readonly prototype: string;
    readonly prototypeNameRegex: RegExp = /__name__/g;
    readonly prototypeLabelRegex: RegExp|null = null;
    readonly prototypePrefix: string;
    private _index: number = 0;

    public constructor(
        readonly $container: HTMLElement,
        readonly prototypeAttributeName: string = 'data-prototype',
    ) {
        if (typeof (this.$container as any)._form_collection !== 'undefined') {
            throw new Error('Collection already initialized.');
        }

        // Get prototype string.
        const prototypeAttr = $container.attributes.getNamedItem(this.prototypeAttributeName);
        if (null === prototypeAttr) {
            throw new Error('The container for the collection does not have any prototype.');
        }
        this.prototype = prototypeAttr.value;

        // Get prototype string.
        const prototypePrefixAttr = $container.attributes.getNamedItem(this.prototypeAttributeName+'-prefix');
        if (null === prototypePrefixAttr) {
            throw new Error('The container for the collection does not have any prototype prefix.');
        }
        this.prototypePrefix = prototypePrefixAttr.value;

        // Get prototype's name.
        const prototypeNameAttr = $container.attributes.getNamedItem(this.prototypeAttributeName+'-name');
        if (null !== prototypeNameAttr) {
            this.prototypeNameRegex = new RegExp(prototypeNameAttr.value, 'g');
        }

        // Get label name if exist.
        const prototypeLabelAttr = $container.attributes.getNamedItem(this.prototypeAttributeName+'-label');
        if (null !== prototypeLabelAttr) {
            this.prototypeLabelRegex = new RegExp(prototypeLabelAttr.value, 'g');
        }

        // Get label name if exist.
        const initCounterAttr = $container.attributes.getNamedItem(this.prototypeAttributeName+'-init-counter');
        if (null !== initCounterAttr) {
            this._index = Number(initCounterAttr.value);
        }

        (this.$container as any)._form_collection = this;
        this.registerDeleteElement(this.$container);
    }

    /**
     * Trigger adding new element.
     */
    public add(): void {
        let prototype = this.prototype;

        if (null !== this.prototypeLabelRegex) {
            prototype = prototype.replace(this.prototypeLabelRegex, (this.index + 1).toString());
        }

        prototype = prototype.replace(this.prototypeNameRegex, this.index.toString());
        this.registerNewElement(htmlToElement(prototype));

        this._index++;
    }

    /**
     * Get the index field number for the collection.
     */
    get index(): number {
        return this._index;
    }

    /**
     * Get the number of element inside the collection.
     */
    get count(): number {
        return this.$container.querySelectorAll(':scope > ['+this.prototypeAttributeName+'-index]').length;
    }

    /**
     * Trigger collection reordering.
     */
    public reorder(): void {
        const event = <CollectionEvent>{
            $container: this.$container,
            $element: null,
            collection: this,
            index: this.index,
        };

        this.applyOrder();
        this.fixName();

        this.$container.dispatchEvent(new CustomEvent('collection-reorder', {
            detail: this.createEvent(),
        }));
    }

    /**
     * Register and add a new element to the collection.
     * @param $prototype
     */
    private registerNewElement($prototype: HTMLElement): void {
        this.$container.appendChild($prototype);
        this.registerDeleteElement($prototype);

        this.$container.dispatchEvent(new CustomEvent('collection-add', {
            detail: this.createEvent($prototype),
        }));

        this.applyOrder();

        setTimeout(() => { // Next tick.
            this.$container.dispatchEvent(new CustomEvent('collection-arrive', {
                detail: this.createEvent($prototype),
            }));

            
        });
    }

    /**
     * Create a CollectionEvent.
     * @param {HTMLElement|null}$element
     */
    private createEvent($element: HTMLElement = null): CollectionEvent {
        return <CollectionEvent>{
            $container: this.$container,
            $element: $element,
            collection: this,
            index: this.index,
        };
    }

    /**
     * Register delete actions.
     * @param {HTMLElement} $section
     */
    private registerDeleteElement($section: HTMLElement): void {
        for (const $element of [].slice.call($section.querySelectorAll<HTMLElement>('['+this.prototypeAttributeName+'-delete]')) as Array<HTMLElement>) {
            const targetSelector = $element.attributes.getNamedItem(this.prototypeAttributeName+'-delete').value;
            const $target: HTMLElement = document.querySelector(targetSelector);

            // Check if target exist.
            if (null === $target) {
                throw new Error('Invalid target, cannot remove.');
            }

            // Register only own collection.
            if ($target.closest('['+this.prototypeAttributeName+']') !== this.$container) {
                continue;
            }

            // Get a deletion confirm message if exist.
            const confirmAttr = $element.attributes.getNamedItem(this.prototypeAttributeName+'-delete-confirm');
            let confirmMessage: string|null = null;
            if (null !== confirmAttr) {
                confirmMessage = confirmAttr.value;
            }

            $element.addEventListener('click', () => {
                if (
                    null === confirmMessage
                    || (
                        null !== confirmMessage
                        && confirm(confirmMessage)
                    )
                ) {
                    const event = <CollectionEvent>{
                        $container: this.$container,
                        $element: $target,
                        collection: this,
                        index: Number($target.getAttribute(this.prototypeAttributeName+'-index')),
                    };

                    this.$container.dispatchEvent(new CustomEvent('collection-delete', {
                        detail: event,
                    }));

                    $target.remove();
                    this.reorder();
                }
            });

            // Remove attribute to prevent reregister.
            $element.removeAttribute(this.prototypeAttributeName+'-delete-confirm');
        }
    }

    /**
     * Update order field with element position.
     */
    private applyOrder(): void {
        for (let i = 0; i < this.$container.children.length; i++) {
            const $child = this.$container.children[i];
            const $orders = $child.querySelectorAll('input['+this.prototypeAttributeName+'-order="' + $child.id + '"]');

            for (const $order of [].slice.call($orders) as Array<HTMLInputElement>) {
                $order.value = i.toString();
            }
        }
    }

    /**
     * Fix name with ordered number included.
     */
    private fixName(): void {
        ([].slice.call(this.$container.querySelectorAll(':scope > ['+this.prototypeAttributeName+'-index]')) as Array<HTMLElement>).forEach(($child: HTMLElement, newIndex) => {
            const indexAttr = $child.attributes.getNamedItem(this.prototypeAttributeName+'-index');
            const oldIndex = Number(indexAttr.value);

            if (oldIndex !== newIndex) {
                indexAttr.value = String(newIndex);

                const prefix = this.prototypePrefix.replace(this.prototypeNameRegex, oldIndex.toString());
                const regex = new RegExp(regxEscape(prefix), 'g');
                const prefixNew = this.prototypePrefix.replace(this.prototypeNameRegex, newIndex.toString());

                for (const $toRename of [].slice.call($child.querySelectorAll('[name^="' + prefix + '"]')) as Array<HTMLElement>) {
                    ($toRename as any).name = ($toRename as any).name.replace(regex, prefixNew);
                }
            }
        });
    }
}
