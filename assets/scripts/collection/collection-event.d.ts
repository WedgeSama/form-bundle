import {Collection} from './collection';

/**
 * Interface for Collection Event.
 */
export interface CollectionEvent {
    collection: Collection;
    $container: HTMLElement|null;
    $element: HTMLElement|null;
    index: number;
}
