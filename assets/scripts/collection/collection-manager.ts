import {Collection} from './collection';
import {CollectionEvent} from './collection-event';

/**
 * Manage multiple collections.
 */
export class CollectionManager {
    private arrives: Array<(event: CollectionEvent) => void> = [];

    public constructor(
        readonly prototypeAttributeName: string = 'data-prototype',
        readonly addButtonAttributeName: string = 'data-prototype-add',
    ) {}

    /**
     * Scan an HTMLElement section to find collection to manage.
     * @param {HTMLElement|string} $section
     */
    public scanForCollection($section: HTMLElement|string): void {
        if (typeof $section === 'string') {
            $section = document.querySelector<HTMLElement>($section);
        }

        for (const $collection of [].slice.call($section.querySelectorAll<HTMLElement>('['+this.prototypeAttributeName+']')) as Array<HTMLElement>) {
            if (this.alreadyCollection($collection)) {
                continue;
            }

            const collection = new Collection($collection, this.prototypeAttributeName);
            $collection.addEventListener('collection-arrive', (event: Event) => {
                if ('detail' in event) {
                    const collectionEvent: CollectionEvent = (event as any).detail;
                    for (const arrive of this.arrives) {
                        arrive(collectionEvent);
                    }

                    // Scan for sub collection.
                    this.scanForCollection(collectionEvent.$element);
                }
            });
        }

        this.scanForAddButton($section);
    }

    /**
     * Scan an HTMLElement section to find collection's adding button.
     * @param {HTMLElement|string} $section
     */
    public scanForAddButton($section: HTMLElement|string): void {
        if (typeof $section === 'string') {
            $section = document.querySelector<HTMLElement>($section);
        }

        for (const $btn of [].slice.call($section.querySelectorAll<HTMLElement>('['+this.addButtonAttributeName+']')) as Array<HTMLElement>) {
            const attr = $btn.attributes.getNamedItem(this.addButtonAttributeName);
            const collection = this.getCollection(attr.value);

            $btn.addEventListener('click', (e: MouseEvent) => {
                collection.add();
            });

            // Remove attribute to prevent rescan.
            $btn.removeAttribute(this.addButtonAttributeName);
        }
    }

    /**
     * Get the Collection object for the corresponding HTMLElement.
     * @param {HTMLElement|string} $element
     */
    public getCollection($element: HTMLElement|string): Collection {
        if (typeof $element === 'string') {
            $element = document.querySelector<HTMLElement>($element);
        }

        if (
            $element instanceof HTMLElement
            && typeof ($element as any)._form_collection !== 'undefined'
        ) {
            return ($element as any)._form_collection;
        }

        throw new Error('There is no collection for given element.');
    }

    /**
     * Add an arrive function to be called when a new element has arrived.
     * @param {Function} arrive
     */
    public addArrive(arrive: (event: CollectionEvent) => void): void
    {
        this.arrives.push(arrive);
    }

    private alreadyCollection($element: HTMLElement): boolean {
        return (typeof ($element as any)._form_collection !== 'undefined');
    }
}
