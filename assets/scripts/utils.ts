/**
 * Escape string for Regex.
 * @param {string} str
 * @return string
 */
export function regxEscape(str: string): string {
    return str.replace(/[-[\]{}()*+!<=:?.\/\\^$|#\s,]/g, '\\$&');
}

/**
 * Create an HTMLElement from a string template.
 * @param {string} html
 * @return HTMLElement|null
 */
export function htmlToElement(html: string): HTMLElement|null {
    const template = document.createElement('template');
    template.innerHTML = html.trim();
    const $child = template.content.firstChild;

    if ($child instanceof HTMLElement) {
        return $child;
    }

    return null;
}
