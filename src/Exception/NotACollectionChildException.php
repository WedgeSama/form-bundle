<?php

/*
 * This file is part of the shop package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Exception;

use Symfony\Component\Form\Exception\LogicException;

/**
 * Class NotACollectionChildException
 *
 * @author Benjamin Georgeault
 */
class NotACollectionChildException extends LogicException
{
}
