<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle;

use WS\Bundle\FormBundle\DependencyInjection\Compiler\CollectionTemplatePass;
use WS\Bundle\FormBundle\DependencyInjection\Compiler\SelectOrCreateTemplatePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class WSFormBundle
 *
 * @author Benjamin Georgeault
 */
class WSFormBundle extends Bundle
{
    /**
     * @inheritDoc
     */
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new CollectionTemplatePass());
        $container->addCompilerPass(new SelectOrCreateTemplatePass());
    }
}
