<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use WS\Bundle\FormBundle\Form\Type\AsyncEntityType;
use WS\Bundle\FormBundle\Form\Type\SelectOrCreateType;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class WSFormExtension
 *
 * @author Benjamin Georgeault
 */
class WSFormExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $childrenErrors = false;

        if ($config['collection']['enabled']) {
            $loader->load('collection.yaml');
            $container->setParameter('ws_form.collection.import_template', $config['collection']['import_template']);
            $container->setParameter('ws_form.collection.main_form_template', $config['collection']['main_form_template']);
        }

        if ($config['collapsable']['enabled']) {
            $loader->load('collapsable.yaml');
            $childrenErrors = true;
        }

        if ($config['tabs']['enabled']) {
            $loader->load('tabs.yaml');
            $childrenErrors = true;
        }

        if ($config['ordering']['enabled']) {
            $loader->load('ordering.yaml');
        }

        if ($config['async']['enabled']) {
            $loader->load('async.yaml');

            $container->setDefinition('ws_form.async.entity_manager', new Definition(EntityManagerInterface::class))
                ->setFactory([new Reference('doctrine'), 'getManager'])
                ->setArguments([
                    $config['async']['doctrine_connection'],
                ])
            ;

            $container->getDefinition(AsyncEntityType::class)
                ->setArgument(0, new Reference('ws_form.async.entity_manager'))
            ;
        }

        if ($config['select_or_create']['enabled']) {
            $container->setParameter('ws_form.select_or_create.import_template', $config['select_or_create']['import_template']);
            $container->setParameter('ws_form.select_or_create.layout_template', $config['select_or_create']['layout_template']);
            $loader->load('select_or_create.yaml');

            $container->setDefinition('ws_form.select_or_create.entity_manager', new Definition(EntityManagerInterface::class))
                ->setFactory([new Reference('doctrine'), 'getManager'])
                ->setArguments([
                    $config['select_or_create']['doctrine_connection'],
                ])
            ;

            $container->getDefinition(SelectOrCreateType::class)
                ->setArgument(0, new Reference('ws_form.select_or_create.entity_manager'))
            ;
        }

        if ($childrenErrors) {
            $loader->load('children_error.yaml');
        }
    }
}
