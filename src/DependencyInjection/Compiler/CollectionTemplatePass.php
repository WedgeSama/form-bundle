<?php
/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class CollectionTemplatePass
 *
 * @author Benjamin Georgeault
 */
class CollectionTemplatePass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasParameter('ws_form.collection.import_template')) {
            return;
        }

        if (true === $container->getParameter('ws_form.collection.import_template')) {
            $resources = $container->getParameter('twig.form.resources');
            $resources[] = '@WSForm/Form/collection_layout.html.twig';
            $container->setParameter('twig.form.resources', $resources);
        }
    }
}
