<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use WS\Bundle\FormBundle\Form\DataMapper\SelectOrCreateCollectionMapper;
use WS\Bundle\FormBundle\Form\DataMapper\SelectOrCreateMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SelectOrCreateType
 *
 * @author Benjamin Georgeault
 */
class SelectOrCreateType extends AbstractType
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if (null !== $data = $form->getData()) {
            $metadata = $this->em->getClassMetadata($options['class']);
            $exist = array_reduce($metadata->getIdentifierValues($data), function ($carry, $id) {
                if (null !== $id) {
                    return true;
                }

                return $carry;
            }, false);

            $view->children[$exist ? 'create' : 'select']->vars['row_attr']['hidden'] = true;
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multiple'] = $options['multiple'];

        $view->vars['select_btn_label'] = $options['select_btn_label'];
        $view->vars['select_btn_label_translation_domain'] = $options['select_btn_label_translation_domain'];
        $view->vars['select_btn_icon'] = $options['select_btn_icon'];
        $view->vars['select_btn_class'] = $options['select_btn_class'];

        $view->vars['create_btn_label'] = $options['create_btn_label'];
        $view->vars['create_btn_label_translation_domain'] = $options['create_btn_label_translation_domain'];
        $view->vars['create_btn_icon'] = $options['create_btn_icon'];
        $view->vars['create_btn_class'] = $options['create_btn_class'];

        $view->vars['single_container_class'] = $options['single_container_class'];
        $view->vars['fields_class'] = $options['fields_class'];
        $view->vars['actions_class'] = $options['actions_class'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['multiple']) {
            $builder
                ->add('collection', CollectionType::class, array_merge([
                        'label' => false,
                    ], $options['collection_options'], [
                        'entry_type' => get_class($builder->getType()->getInnerType()),
                        'entry_options' => array_merge($options, [
                            'multiple' => false,
                            'collection_options' => [],
                        ], $options['collection_options']['entry_options'] ?? []),
                    ],
                ))
                ->setDataMapper(new SelectOrCreateCollectionMapper())
            ;
        } else {
            $builder
                ->add('select', $options['select_type'], array_merge($options['select_options'], [
                    'class' => $options['class'],
                    'row_attr' => array_merge($options['select_options']['row_attr'] ?? [], [
                        'data-select-or-create--select' => true,
                    ]),
                ]))
                ->add('create', $options['create_type'], array_merge($options['create_options'], [
                    'data_class' => $options['class'],
                    'row_attr' => array_merge($options['create_options']['row_attr'] ?? [], [
                        'data-select-or-create--create' => true,
                    ]),
                ]))
                ->add('state', ChoiceType::class, [
                    'label' => false,
                    'choices' => [
                        'select' => 'select',
                        'create' => 'create',
                    ],
                    'row_attr' => [
                        'hidden' => true,
                    ],
                    'attr' => [
                        'data-select-or-create--state' => true,
                    ],
                    'translation_domain' => false,
                ])
                ->setDataMapper(new SelectOrCreateMapper(
                    $this->em->getClassMetadata($options['class']),
                ))
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'multiple' => false,
            'error_bubbling' => false,
            'select_type' => EntityType::class,
            'select_options' => [],
            'create_type' => null,
            'create_options' => [],
            'collection_options' => [],

            'select_btn_label' => 'ws_form.select_or_create.select_btn_label',
            'select_btn_label_translation_domain' => null,
            'select_btn_icon' => false,
            'select_btn_class' => false,

            'create_btn_label' => 'ws_form.select_or_create.create_btn_label',
            'create_btn_label_translation_domain' => null,
            'create_btn_icon' => false,
            'create_btn_class' => false,

            'single_container_class' => false,
            'fields_class' => false,
            'actions_class' => false,
        ]);

        $resolver
            ->setRequired('class')
            ->setAllowedTypes('class', 'string')
            ->addNormalizer('data_class', function (Options $options, $value) {
                return $options['class'];
            })
        ;

        $resolver
            ->setAllowedTypes('select_options', 'array')
            ->setAllowedTypes('create_options', 'array')
            ->setRequired('create_type')
            ->setAllowedTypes('create_type', 'string')
            ->setRequired('select_type')
            ->setAllowedTypes('select_type', 'string')
            ->setAllowedTypes('multiple', 'bool')
        ;

        $resolver
            ->setAllowedTypes('collection_options', 'array')
            ->addNormalizer('collection_options', function (Options $options, $value) {
                if (!$options['multiple'] && !empty($value)) {
                    throw new InvalidConfigurationException(sprintf(
                        'The option "collection_options" for "%s" must be empty if "multiple" option is false.',
                        self::class,
                    ));
                }

                return $value;
            })
        ;

        $resolver
            ->addNormalizer('row_attr', function (Options $options, $value) {
                if (!$options['multiple']) {
                    $value = (array) $value;
                    $value['data-select-or-create'] = true;
                }

                return $value;
            })
        ;

        $resolver
            ->addAllowedTypes('select_btn_label', ['boolean', 'string'])
            ->addAllowedTypes('select_btn_label_translation_domain', ['null', 'boolean', 'string'])
            ->addAllowedTypes('select_btn_icon', ['boolean', 'string'])
            ->addAllowedTypes('select_btn_class', ['boolean', 'string'])
            ->addAllowedTypes('create_btn_label', ['boolean', 'string'])
            ->addAllowedTypes('create_btn_label_translation_domain', ['null', 'boolean', 'string'])
            ->addAllowedTypes('create_btn_icon', ['boolean', 'string'])
            ->addAllowedTypes('create_btn_class', ['boolean', 'string'])
        ;

        $translationCallback = function (Options $options, $value) {
            if (null === $value) {
                return $options['translation_domain'] ?? 'messages';
            }

            return $value;
        };

        $resolver->setNormalizer('select_btn_label_translation_domain', $translationCallback);
        $resolver->setNormalizer('create_btn_label_translation_domain', $translationCallback);

        $resolver
            ->addAllowedTypes('single_container_class', ['boolean', 'string'])
            ->addAllowedTypes('fields_class', ['boolean', 'string'])
            ->addAllowedTypes('actions_class', ['boolean', 'string'])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'ws_select_or_create';
    }
}
