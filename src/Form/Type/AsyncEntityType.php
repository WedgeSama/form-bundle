<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Persistence\Mapping\MappingException;
use WS\Bundle\FormBundle\Form\DataTransformer\AsyncEntitiesToValuesTransformer;
use WS\Bundle\FormBundle\Form\DataTransformer\AsyncEntityToValueTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\Extension\Core\DataTransformer\ChoicesToValuesTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\ChoiceToValueTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AsyncEntityType
 *
 * @author Benjamin Georgeault
 */
class AsyncEntityType extends AbstractType
{
    private EntityManagerInterface $em;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $options = $form->getConfig()->getOptions();
            if (!$options['pre_submit_called'] && null !== $parent = $form->getParent()) {
                $parent->add($form->getName(), get_class($this), array_merge($options, [
                    'pre_submit_called' => true,
                    'choices' => $this->getChoices($options, $data = $event->getData()),
                ]));
                $newForm = $parent->get($form->getName());
                $newForm->submit($data);
            }
        }, 50);

        // Replace original Symfony's choice transformers.
        $viewTransformers = $builder->getViewTransformers();
        $builder->resetViewTransformers();
        foreach ($viewTransformers as $transformer) {
            if ($transformer instanceof ChoiceToValueTransformer) {
                $builder->addViewTransformer(new AsyncEntityToValueTransformer($options['entity_repository'], $options['entity_metadata']));
                continue;
            }

            if ($transformer instanceof ChoicesToValuesTransformer) {
                $builder->addViewTransformer(new AsyncEntitiesToValuesTransformer($options['entity_repository'], $options['entity_metadata']));
                continue;
            }

            $builder->addViewTransformer($transformer);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => [],
            'choice_value' => 'id',
            'pre_submit_called' => false,
            'choice_serialization_groups' => 'Default',
            'entity_repository' => null, // You cannot set this option.
            'entity_metadata' => null, // You cannot set this option.
            'url_html_attr_name' => 'data-remote-url',
            'serialize_option_attr_name' => 'data-option-data',
        ]);

        $resolver
            ->setAllowedTypes('url_html_attr_name', 'string')
            ->setNormalizer('url_html_attr_name', function (Options $options, $value) {
                if (empty($value)) {
                    throw new InvalidConfigurationException(sprintf(
                        'The option "url_html_attr_name" for "%s" cannot be empty.',
                        self::class,
                    ));
                }

                return $value;
            })
        ;

        $resolver
            ->setAllowedTypes('serialize_option_attr_name', 'string')
            ->setNormalizer('serialize_option_attr_name', function (Options $options, $value) {
                if (empty($value)) {
                    throw new InvalidConfigurationException(sprintf(
                        'The option "serialize_option_attr_name" for "%s" cannot be empty.',
                        self::class,
                    ));
                }

                return $value;
            })
        ;

        $resolver
            ->setAllowedTypes('choice_serialization_groups', ['string', 'string[]'])
            ->setNormalizer('choice_serialization_groups', function (Options $options, $value) {
                if (empty($value)) {
                    throw new InvalidConfigurationException(sprintf(
                        'The option "choice_serialization_groups" for "%s" cannot be empty.',
                        self::class,
                    ));
                }

                return (array) $value;
            })
            ->addNormalizer('choice_attr', function (Options $options, $value) {
                $serializationGroups = $options['choice_serialization_groups'];

                return function($choice, $key, $choiceValue) use ($value, $serializationGroups, $options) {
                    if (null === $value) {
                        $value = [];
                    }

                    if (is_callable($value)) {
                        $attr = $value($choice, $key, $choiceValue);
                    } elseif (is_array($value)) {
                        $attr = $value;
                    } else {
                        throw new InvalidConfigurationException(sprintf(
                            'The given option "choice_attr" for "%s" must be callable or an array only, "%s" given.',
                            self::class,
                            gettype($value),
                        ));
                    }

                    return array_merge($attr, [
                        $options['serialize_option_attr_name'] => $this->serializer->serialize($choice, 'json', [
                            'groups' => $serializationGroups,
                        ]),
                    ]);
                };
            })
        ;

        $resolver
            ->setRequired('async_url')
            ->setAllowedTypes('async_url', 'string')
            ->addNormalizer('attr', function (Options $options, $value) {
                return array_merge($value, [
                    $options['url_html_attr_name'] => $options['async_url'],
                ]);
            })
        ;

        $resolver
            ->setAllowedTypes('pre_submit_called', 'boolean')
        ;

        $resolver
            ->setRequired('class')
            ->setAllowedTypes('class', 'string')
            ->addNormalizer('class', function (Options $options, $value) {
                if (!class_exists($value) && !interface_exists($value)) {
                    throw new InvalidConfigurationException(sprintf(
                        'The given option "class" for "%s" must be a valid class or interface.',
                        self::class,
                    ));
                }

                return $value;
            })
        ;

        $resolver->setNormalizer('entity_repository', function (Options $options) {
            try {
                return $this->em->getRepository($options['class']);
            } catch (MappingException $e) {
                throw new InvalidConfigurationException(sprintf(
                    'Cannot find mapping for entity "%s".',
                    $options['class'],
                ), 0, $e);
            }
        });

        $resolver->setNormalizer('entity_metadata', function (Options $options) {
            try {
                $metadata = $this->em->getClassMetadata($options['class']);
            } catch (MappingException $e) {
                throw new InvalidConfigurationException(sprintf(
                    'Cannot find mapping for entity "%s".',
                    $options['class'],
                ), 0, $e);
            }

            if ($metadata->isIdentifierComposite) {
                throw new InvalidConfigurationException(sprintf(
                    'The entity "%s" has composite identifier. The "%s" does not support it.',
                    $options['class'],
                    self::class,
                ));
            }

            return $metadata;
        });
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'ws_async_entity';
    }

    private function getChoices(array $options, $data): array
    {
        if ($data instanceof PersistentCollection) {
            return $data->toArray();
        }

        if (null === $data) {
            return [];
        }

        return $options['entity_repository']->findBy(['id' => $data]);
    }
}
