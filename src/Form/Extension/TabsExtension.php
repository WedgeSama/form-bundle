<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TabsExtension
 *
 * @author Benjamin Georgeault
 */
class TabsExtension extends AbstractTypeExtension
{
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['tabs'] = $options['tabs'];

        if ($options['tabs']) {
            $view->vars['tabsbar_class'] = $options['tabsbar_class'];
            $view->vars['tabsbar_attr'] = $options['tabsbar_attr'];
            $view->vars['tabsbar_item_class'] = $options['tabsbar_item_class'];
            $view->vars['tabsbar_item_active_class'] = $options['tabsbar_item_active_class'];
            $view->vars['tabsbar_item_attr'] = $options['tabsbar_item_attr'];
            $view->vars['tabsbar_item_button_class'] = $options['tabsbar_item_button_class'];
            $view->vars['tabsbar_item_button_target_attr_name'] = $options['tabsbar_item_button_target_attr_name'];
            $view->vars['tabsbar_item_button_attr'] = $options['tabsbar_item_button_attr'];

            $view->vars['tabs_panels_class'] = $options['tabs_panels_class'];
            $view->vars['tabs_panels_attr'] = $options['tabs_panels_attr'];
            $view->vars['tabs_panel_class'] = $options['tabs_panel_class'];
            $view->vars['tabs_panel_active_class'] = $options['tabs_panel_active_class'];
            $view->vars['tabs_panel_attr'] = $options['tabs_panel_attr'];

            $view->vars['tabs_items'] = [];
            $toOpen = null;
            foreach ($view->children as $key => $child) {
                $view->vars['tabs_items'][$key] = [
                    'id' => $child->vars['id'],
                    'text' => $child->vars['name'],
                ];

                if ($child->vars['contain_errors']) {
                    $view->vars['tabs_items'][$key]['btn_class'] = $options['tabsbar_item_button_error_class'];

                    if (null === $toOpen) {
                        $toOpen = $key;
                    }
                }
            }

            if (null === $toOpen) {
                $toOpen = array_key_first($view->children);
            }

            $view->vars['tabs_items'][$toOpen]['open'] = true;
            $view->vars['tabs_items'][$toOpen]['btn_class'] =
                ($view->vars['tabs_items'][$toOpen]['btn_class'] ?? '') .
                ' ' . $options['tabsbar_item_active_class']
            ;

            $view->children[$toOpen]->vars['tabs_default_open'] = true;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'tabs' => false,

            'tabsbar_class' => '',
            'tabsbar_attr' => [],
            'tabsbar_item_class' => '',
            'tabsbar_item_active_class' => '',
            'tabsbar_item_error_class' => '',
            'tabsbar_item_attr' => [],
            'tabsbar_item_button_class' => '',
            'tabsbar_item_button_error_class' => '',
            'tabsbar_item_button_target_attr_name' => '',
            'tabsbar_item_button_attr' => [],

            'tabs_panels_class' => '',
            'tabs_panels_attr' => [],
            'tabs_panel_class' => '',
            'tabs_panel_active_class' => '',
            'tabs_panel_attr' => [],
        ]);

        $resolver->addAllowedTypes('tabs', ['boolean']);

        $resolver->addAllowedTypes('tabsbar_class', ['string']);
        $resolver->addAllowedTypes('tabsbar_attr', ['array']);
        $resolver->addAllowedTypes('tabsbar_item_class', ['string']);
        $resolver->addAllowedTypes('tabsbar_item_active_class', ['string']);
        $resolver->addAllowedTypes('tabsbar_item_attr', ['array']);
        $resolver->addAllowedTypes('tabsbar_item_button_class', ['string']);
        $resolver->addAllowedTypes('tabsbar_item_button_target_attr_name', ['string']);
        $resolver->addAllowedTypes('tabsbar_item_button_attr', ['array']);

        $resolver->addAllowedTypes('tabs_panels_class', ['string']);
        $resolver->addAllowedTypes('tabs_panels_attr', ['array']);
        $resolver->addAllowedTypes('tabs_panel_class', ['string']);
        $resolver->addAllowedTypes('tabs_panel_active_class', ['string']);
        $resolver->addAllowedTypes('tabs_panel_attr', ['array']);
    }

    public static function getExtendedTypes(): iterable
    {
        yield CollectionType::class;
    }
}
