<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderingCollectionExtension
 *
 * @author Benjamin Georgeault
 */
class OrderingCollectionExtension extends AbstractTypeExtension
{

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['ordering']) {
            $view->vars['ordering'] = true;
            $view->vars['attr']['data-prototype-ordering'] = '';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'ordering' => false,
            'ordering_field_mapped' => true,
            'ordering_field' => 'ordering',
        ]);

        $resolver->addAllowedTypes('ordering', 'boolean');
        $resolver->addAllowedTypes('ordering_field_mapped', 'boolean');
        $resolver->addAllowedTypes('ordering_field', 'string');

        $resolver->setNormalizer('entry_options', function (Options $options, $value) {
            return array_merge($value ?? [], [
                'ordering_inner' => $options['ordering'],
                'ordering_inner_field_mapped' => $options['ordering_field_mapped'],
                'ordering_inner_field' => $options['ordering_field'],
            ]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [
            CollectionType::class,
        ];
    }
}
