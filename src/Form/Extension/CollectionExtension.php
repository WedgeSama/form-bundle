<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CollectionExtension
 *
 * @author Benjamin Georgeault
 */
class CollectionExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if (array_key_exists('prototype', $view->vars)) {
            $view->vars['attr']['data-prototype-name'] = $options['prototype_name'];
            $view->vars['attr']['data-prototype-label'] = $options['prototype_name'].'label__';
            $view->vars['attr']['data-prototype-init-counter'] = $form->count();
            $view->vars['attr']['data-prototype-prefix'] = $view->vars['prototype']->vars['full_name'];
        }

        $view->vars['name_label'] = $options['prototype_name'].'label__';

        if ($options['allow_add']) {
            $view->vars['add_btn_label'] = $options['add_btn_label'];
            $view->vars['add_btn_label_translation_domain'] = $options['add_btn_label_translation_domain'];
            $view->vars['add_btn_icon'] = $options['add_btn_icon'];
            $view->vars['add_btn_class'] = $options['add_btn_class'];
        }

        if ($options['allow_delete']) {
            $view->vars['delete_btn_label'] = $options['delete_btn_label'];
            $view->vars['delete_btn_label_translation_domain'] = $options['delete_btn_label_translation_domain'];
            $view->vars['delete_btn_icon'] = $options['delete_btn_icon'];
            $view->vars['delete_btn_class'] = $options['delete_btn_class'];
            $view->vars['delete_btn_confirm'] = $options['delete_btn_confirm'];
            $view->vars['delete_btn_confirm_translation_domain'] = $options['delete_btn_confirm_translation_domain'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'add_btn_label' => 'ws_form.collection.add_btn_label',
            'add_btn_label_translation_domain' => null,
            'add_btn_icon' => false,
            'add_btn_class' => false,

            'delete_btn_label' => 'ws_form.collection.delete_btn_label',
            'delete_btn_label_translation_domain' => null,
            'delete_btn_icon' => false,
            'delete_btn_class' => false,
            'delete_btn_confirm' => 'ws_form.collection.delete_btn_confirm',
            'delete_btn_confirm_translation_domain' => null,
        ]);

        $resolver->addAllowedTypes('add_btn_label', ['boolean', 'string']);
        $resolver->addAllowedTypes('add_btn_label_translation_domain', ['null', 'boolean', 'string']);
        $resolver->addAllowedTypes('add_btn_icon', ['boolean', 'string']);
        $resolver->addAllowedTypes('add_btn_class', ['boolean', 'string']);

        $resolver->addAllowedTypes('delete_btn_label', ['boolean', 'string']);
        $resolver->addAllowedTypes('delete_btn_label_translation_domain', ['null', 'boolean', 'string']);
        $resolver->addAllowedTypes('delete_btn_icon', ['boolean', 'string']);
        $resolver->addAllowedTypes('delete_btn_class', ['boolean', 'string']);
        $resolver->addAllowedTypes('delete_btn_confirm', ['boolean', 'string']);
        $resolver->addAllowedTypes('delete_btn_confirm_translation_domain', ['null', 'boolean', 'string']);

        $resolver->setNormalizer('entry_options', function (Options $options, $value) {
            if (!array_key_exists('label', $value)) {
                $value['label'] = 'ws_form.collection.item_label';
            }

            return $value;
        });

        $translationCallback = function (Options $options, $value) {
            if (null === $value) {
                return $options->offsetGet('translation_domain') ?? 'messages';
            }

            return $value;
        };

        $resolver->setNormalizer('add_btn_label_translation_domain', $translationCallback);
        $resolver->setNormalizer('delete_btn_label_translation_domain', $translationCallback);
        $resolver->setNormalizer('delete_btn_confirm_translation_domain', $translationCallback);
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [
            CollectionType::class,
        ];
    }
}
