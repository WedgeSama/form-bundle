<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class FormTemplateExtension
 *
 * @author Benjamin Georgeault
 */
class FormTemplateExtension extends AbstractTypeExtension
{
    /**
     * @var string
     */
    private $formTemplate;

    /**
     * FormTemplateExtension constructor.
     * @param string $formTemplate
     */
    public function __construct(string $formTemplate)
    {
        $this->formTemplate = $formTemplate;
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['ws_form__form_template'] = $this->formTemplate;
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [
            FormType::class,
        ];
    }
}
