<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class CollectionInnerExtension
 *
 * @author Benjamin Georgeault
 */
class CollectionInnerExtension extends AbstractTypeExtension
{
    use CollectionTrait;

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if ($this->isInCollection($form)) {
            $parentConfig = $form->getParent()->getConfig();
            $view->vars['collection_allow_delete'] = $parentConfig->getOption('allow_delete', false);
            $view->vars['collection_delete_btn_label'] = $parentConfig->getOption('delete_btn_label', false);
            $view->vars['collection_delete_btn_label_translation_domain'] = $parentConfig->getOption('delete_btn_label_translation_domain', false);
            $view->vars['collection_delete_btn_icon'] = $parentConfig->getOption('delete_btn_icon', false);
            $view->vars['collection_delete_btn_class'] = $parentConfig->getOption('delete_btn_class', false);
            $view->vars['collection_delete_btn_confirm'] = $parentConfig->getOption('delete_btn_confirm', false);
            $view->vars['collection_delete_btn_confirm_translation_domain'] = $parentConfig->getOption('delete_btn_confirm_translation_domain', false);
            $view->vars['attr']['data-prototype-index'] = $view->vars['name'];

            $view->vars['row_attr']['id'] =
                $view->vars['collection_delete_id'] =
                $view->vars['id'].'__collection_item'
            ;

            $translationVars = [];
            if (is_int($key = $this->getParentKey($form))) {
                $translationVars['%collection_number%'] = $this->isPrototype($form) ?
                    ($parentConfig->getOption('prototype_name').'label__') :
                    ($key + 1)
                ;
            } else {
                $translationVars['%collection_key%'] = $this->isPrototype($form) ?
                    ($parentConfig->getOption('prototype_name').'label__') :
                    $key
                ;
            }

            if (array_key_exists('label_translation_parameters', $view->vars)) {
                $view->vars['label_translation_parameters'] = array_merge($view->vars['label_translation_parameters'], $translationVars);
            }

            if (array_key_exists('attr_translation_parameters', $view->vars)) {
                $view->vars['attr_translation_parameters'] = array_merge($view->vars['attr_translation_parameters'], $translationVars);
            }

            if (array_key_exists('help_translation_parameters', $view->vars)) {
                $view->vars['help_translation_parameters'] = array_merge($view->vars['help_translation_parameters'], $translationVars);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($view->vars['in_collection'] = $this->isInCollection($form)) {
            array_splice(
                $view->vars['block_prefixes'],
                max(count($view->vars['block_prefixes']) - 2, 0),
                0,
                'collection_element'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [
            FormType::class,
        ];
    }

    /**
     * @param FormInterface $form
     * @return false|int|string
     */
    private function getParentKey(FormInterface $form)
    {
        return array_search($form, $form->getParent()->all());
    }
}
