<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Bundle\FormBundle\Form\DataMapper;

use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;

/**
 * Class SelectOrCreateCollectionMapper
 *
 * @author Benjamin Georgeault
 */
class SelectOrCreateCollectionMapper implements DataMapperInterface
{
    public function mapDataToForms($viewData, \Traversable $forms): void
    {
        if (empty($viewData)) {
            return;
        }

        if (!is_iterable($viewData)) {
            throw new UnexpectedTypeException($viewData, 'iterable');
        }

        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);

        $forms['collection']->setData($viewData);
    }

    public function mapFormsToData(\Traversable $forms, &$viewData): void
    {
        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);

        $viewData = $forms['collection']->getData();
    }
}
