Using the library in Symfony Form
=================================

## Symfony side
### In FormType

Use it as usual `CollectionType`:

```php
<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class ExampleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('foobar', CollectionType::class, [
                // Options from official doc + options bellow.
            ])
            // ...
        ;
    }
    // ...
}
```

### Available options

The bundle come with a Twig template to automatically inject adding and removing buttons. You can customize those
buttons with some options:

Option | Default value | Allowed Values | Description
------ | ------------- | -------------- | -----------
add_btn_label | `ws_form.collection.add_btn_label` | `string` or `false` | Set or disable the label for the adding button.
add_btn_label_translation_domain | `messages` | `string`, `null` or `false` | Set or disable the usage of translation domain for the adding button.
add_btn_icon | `false` | `string` or `false` | Set or disable an icon like Font Awesome (using `<i class="value"></i>`)
add_btn_class | `false` | `string` or `false` | Set or disable a CSS class on the adding button.
delete_btn_label | `ws_form.collection.delete_btn_label` | `string` or `false` | Set or disable the label for the removing button.
delete_btn_label_translation_domain | `messages` | `string`, `null` or `false` | Set or disable the usage of translation domain for the removing button.
delete_btn_icon | `false` | `string` or `false` | Set or disable an icon like Font Awesome (using `<i class="value"></i>`)
delete_btn_class | `false` | `string` or `false` | Set or disable a CSS class on the removing button.
delete_btn_confirm | `ws_form.collection.delete_btn_confirm` | `string` or `false` | Set or disable a confirm dialog on removing button.
delete_btn_confirm_translation_domain | `messages` | `string`, `null` or `false` | Set or disable the usage of translation domain for the removing confirm dialog.

### "Global setting"

You can use the power of Symfony's Form extension to set the previous listed options for all CollectionType usage.
Bellow an example using Bootstrap and Font Awesome:
```php
<?php
namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionExtension extends AbstractTypeExtension
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'add_btn_icon' => false,
            'add_btn_class' => 'btn btn-primary',
            'delete_btn_label' => false,
            'delete_btn_label_translation_domain' => false,
            'delete_btn_icon' => 'far fa-trash-alt',
            'delete_btn_class' => 'btn btn-danger',
        ]);
    }

    public static function getExtendedTypes(): iterable
    {
        yield CollectionType::class;
    }
}
```

## Front side

Import the library to your project and initialize the collection manager.
```typescript
import {CollectionEvent, CollectionManager} from '@wedgesama/form-collection';

const collectionManager = new CollectionManager();

/** Your function use to init js form element */
function arrive($element: HTMLElement) {
    // Do your js init for an html element.
    // ...
    // Call the collection scan on the element to handle sub collection.
    collectionManager.scanForCollection($element);
    // ...
}

// Add a callback to the collection manager to 
collectionManager.addArrive((event: CollectionEvent) => {
    // The event.$element refering to new HTML element.
    if (null !== event.$element) {
        arrive(event.$element);
    }
});
```
