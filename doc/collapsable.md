Enable collapsable capability
=============================

The library allow to use a collapsable mechanism to hide/show part of your form.

## Symfony side

```php
<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class ExampleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('foobar', AnotherType::class, [
                'collapsable' => true,
                'collapsable_initial_state' => true, // Is the form open on page loading.
            ])
            // ...
        ;
    }
    // ...
}
```

## Front side

Add this code in your script, example based on [front doc](front.md).

```typescript
import {CollapsableManager} from '@wedgesama/form-collection';

function arrive($element: HTMLElement) {
    // ...
    collapsableManager.scanForCollapsable($element);
    // ...
}
```
