Enable tabs capability
======================

## Inside Collection

Use it as usual `CollectionType` with `tabs` option set to `true`:

```php
<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class ExampleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('foobar', CollectionType::class, [
                'tabs' => true,
                // Options from official doc + options bellow.
            ])
            // ...
        ;
    }
    // ...
}
```

## Standalone Tab in form

TODO, not yet implemented. Only CollectionType for now.

## Available options

The bundle come with a Twig template to automatically inject tabs with buttons. You can customize those tabs and
buttons with some options:

Option | Default value | Allowed Values | Description
------ |---------------|----------------| -----------
tabs | `false`       | `boolean`      | Enable or disable tabs.
tabsbar_class | `''`          | `string`       | Set class for the tabs' bar container.
tabsbar_attr | `[]`          | `array`        | Set attr for the tabs' bar container.
tabsbar_item_class | `''`          | `string`       | Set class for all the tabs' bar items.
tabsbar_item_active_class | `''`          | `string`       | Set class for the tabs' bar active item.
tabsbar_item_error_class | `''`          | `string`       | Set class for the tabs' bar items containing errors.
tabsbar_item_attr | `[]`         | `array`        | Set attr for all the tabs' bar items.
tabsbar_item_button_class | `''`          | `string`       | Set button class for all the tabs' bar items.
tabsbar_item_button_error_class | `''`          | `string`       | Set button class for all the tabs' bar items containing errors.
tabsbar_item_button_target_attr_name | ``          | `string`       | Set button target attr name for all the tabs' bar items.
tabsbar_item_button_attr | `[]`          | `array`       | Set button attr for all the tabs' bar items.
tabs_panels_class | `''`          | `string`       | Set class for the tabs' panels container.
tabs_panels_attr | `[]`          | `array`        | Set attr for the tabs' panels container.
tabs_panel_class | `''`          | `string`       | Set class for the tabs' all panels.
tabs_panel_active_class | `''`          | `string`       | Set class for the active tabs' panel.
tabs_panel_attr | `[]`          | `array`        | Set attr for the tabs' all panels.


## "Global setting"

You can use the power of Symfony's Form extension to set the previous listed options for all your tabs usage.
Bellow an example using Bootstrap:
```php
<?php
namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionExtension extends AbstractTypeExtension
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'tabsbar_class' => 'nav nav-tabs',
            'tabsbar_item_class' => 'nav-item',
            'tabsbar_item_active_class' => 'active',
            'tabsbar_item_button_class' => 'nav-link',
            'tabsbar_item_button_error_class' => 'text-danger',
            'tabsbar_item_button_attr' => [
                'data-bs-toggle' => 'tab',
            ],
            'tabsbar_item_button_target_attr_name' => 'data-bs-target',
            'tabs_panels_class' => 'tab-content',
            'tabs_panel_class' => 'tab-pane fade',
            'tabs_panel_active_class' => 'active show',
        ]);
    }

    public static function getExtendedTypes(): iterable
    {
        yield CollectionType::class;
    }
}
```

## Front side

We do not provide JS implementation. Use the one from the library you use (eg. Bootstrap) or make your own.
