WSFormBundle
============

A Symfony bundle to extends Form capabilities.

## Install

### Install asset package

- Require the package
```
yarn add @wedgesama/form-collection
npm install @wedgesama/form-collection
```

### Install Symfony's bundle (^4.2|^5.0)

- Require the package
```
composer require wedgesama/form-bundle
```
- Enable the bundle in your `config/bundle.php`:
```php
<?php

return [
    //...
    WS\Bundle\FormBundle\WSFormBundle::class => ['all' => true],
    //...
];
```

## Documentations

- [Collection](doc/collection.md)
- [Collapsable](doc/collapsable.md)
- [Tabs](doc/tabs.md)

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
