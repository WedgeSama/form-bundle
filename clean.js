const del = require('del');

del.sync([
    'bundles',
    'esm5',
    'esm2015',
    'styles.css',
    'styles.css.map',
]);
